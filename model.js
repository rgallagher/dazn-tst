'use strict';

const Redis = require("redis");
const Client = Redis.createClient({
  url: process.env.REDIS_URL,
});

const {promisify} = require('util');

const internals = {
  getAsync: promisify(Client.get).bind(Client),
  setAsync: promisify(Client.set).bind(Client),
};

/**
 * Return the current views for a user
 *
 * @param {string} userId 
 */
internals.getViews = async (userId) => {
  const key = `views:${userId}`;

  let views;

  try {
    views = await internals.getAsync(key);
  } catch (error) {
    return Promise.reject(error);
  }

  if (!views){
    return [];
  }

  return views.split(',');
};

/**
 * Set viewing for a user
 *
 * @param {*} userId 
 * @param {*} viewing 
 */
internals.setViews = async (userId, viewing) => {
  const key = `views:${userId}`;

  try {
    await internals.setAsync(key, viewing.join(','));
  } catch (error) {
    return Promise.reject(error);
  }
};

module.exports = internals;
