'use strict';

const Hapi = require('hapi');
const Routes = require('./routes');

const server = Hapi.server({
  host: process.env.HOST || '0.0.0.0',
  port: process.env.PORT || 3000,
});

const init = async () => {
  // Initialise the main routes
  Routes.init(server);

  await server.start();

  console.log(`Server running at: ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {
  process.exit(1);
});

init();