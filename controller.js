'use strict';

const Model = require('./model');

const internals = {};

/**
 * Controller for the `stats` endpoint
 *
 * @param {*} request 
 * @param {*} h 
 */
internals.stats = async (request, h) => {
  const userId = request.params.user_id;

  let viewing;

  try {
    viewing = await Model.getViews(userId);
  } catch (error) {
    return h.response(`${error}`).code(500);
  }

  const stats = {
    user: userId,
    viewing,
  };

  return stats;
};

/**
 * Controller for the `watch` endpoint
 *
 * @param {*} request 
 * @param {*} h 
 */
internals.watch = async (request, h) => {
  const userId = request.params.user_id;
  const videoId = (request.payload || {}).video_id;

  if (!videoId) {
    return h.response('No video ID provided').code(400);
  }

  let viewing;

  try {
    viewing = await Model.getViews(userId);
  } catch (error) {
    return h.response(`${error}`).code(500);
  }

  if (viewing.includes(videoId)) {
    return { status: 'viewing' };
  }

  if (viewing.length >= 3) {
    return { status: 'denied' };
  }

  viewing.push(videoId);

  try {
    await Model.setViews(userId, viewing);
  } catch (error) {
    return h.response(`${error}`).code(500);
  }

  return { status: 'allowed' };
};

/**
 * Controller for the `stop` endpoint
 * 
 * @param {*} request 
 * @param {*} h 
 */
internals.stop = async (request, h) => {
  const userId = request.params.user_id;
  const videoId = (request.payload || {}).video_id;

  if (!videoId) {
    return h.response('No video ID provided').code(400);
  }

  let viewing;

  try {
    viewing = await Model.getViews(userId);
  } catch (error) {
    return h.response(`${error}`).code(500);
  }

  if (!viewing.includes(videoId)) {
    return { status: 'denied' };
  }

  viewing.splice(viewing.indexOf(videoId), 1);

  try {
    await Model.setViews(userId, viewing);
  } catch (error) {
    return h.response(`${error}`).code(500);
  }

  return { status: 'stopped' };
};

module.exports = internals;
