'use strict';

const Controller = require('./controller');
const Package = require('./package.json');

const internals = {};

internals.init = (server) => {

  server.route({
    method: 'GET',
    path: '/',
    handler: (request, h) => {
      return { Name: Package.name, Version: Package.version };
    }
  });

  server.route({
    method: 'GET',
    path: '/stats/{user_id}',
    handler: Controller.stats,
  });

  server.route({
    method: 'POST',
    path: '/watch/{user_id}',
    handler: Controller.watch,
  });

  server.route({
    method: 'POST',
    path: '/stop/{user_id}',
    handler: Controller.stop,
  });

};  

module.exports = internals;
