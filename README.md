# View Service (Test)

Build a service in Node.js that exposes an API which can be consumed from any
client. This service must check how many video streams a given user is watching
and prevent a user watching more than 3 video streams concurrently.

A live version of this service can be found at: `https://dazn-test.herokuapp.com`

### Building
To build and run  at least Node version 8 is required and a local Redis server.

* Install Grant task runner globally: `npm install -g grunt-cli`
* Install Node dependencies `npm install` 
* Start the server `grunt local`

### Endpoints:

* `GET /stats/{user_id}` Retrieve info on which video's a user is watching
* `POST /watch/{user_id}` Request authorization to view a video by a user
  * Post json body must contain a `video_id` (this can be any string value for now)
  * Response body will return a status of either
    * `allowed` - The user is authorized to view the video 
    * `viewing` - The user is already viewing the video
    * `denied` - The user is noauthorized to view the video
* `POST /stop/{user_id}` Report user has stopped watching video
  * Post json body must contain a `video_id` (this can be any string value for now)
  * Response body will return a status of either
    * `stopped` - The user has stopped viewing the video
    * `denied` - The user was not viewing the video

Example Request (using the <https://httpie.org> library):

Get stats: 
``` shell
$ http GET localhost:3000/stats/123
```

Request to watch:
``` shell
$ http POST localhost:3000/watch/123 video_id=456
```

Report stopped watching:
``` shell
$ http POST localhost:3000/stop/123 video_id=456
```

### Scaling

The service was designed to be stateless and perform one task only, using a Redis backend to store which video's a user is viewing. This allows the Service and Redis server to be scaled independently. 

The service also does not provide any authentication method as this would be provided by another `users` microservice, which would be independently scaled as well.
