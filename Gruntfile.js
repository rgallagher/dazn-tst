'use strict';

const WatchPaths = ['index.js', 'routes.js', 'controller.js', 'model.js'];

module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    env: {
      dev: {
        src: './env/local.env'
      },
    },
    nodemon: {
      run: {
        script: 'index.js',
        options: {
          watch: WatchPaths,
        }
      },
      debug: {
        script: 'index.js',
        options: {
          nodeArgs: ['--debug', '--trace_gc'],
          watch: WatchPaths
        }
      }
    },

  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-env');

  // Default task - jshint once at the start, then nodemon
  grunt.registerTask('default', ['env:dev', 'nodemon:run']);
  grunt.registerTask('local', ['env:dev', 'nodemon:run']);
};